"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const axios_1 = __importDefault(require("axios"));
class RandomDogsCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "dog",
            aliases: ["random_dog", "anass"],
            group: "random",
            memberName: "dog",
            description: "GIVES U EZ CAT",
        });
    }
    run(msg) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield axios_1.default.get("https://dog.ceo/api/breeds/image/random");
                const message = msg.say("LOOKING FOR 3BYA");
                yield msg.say({
                    files: [response.data.message]
                });
                yield message.then((response) => {
                    response.delete();
                });
            }
            catch (e) {
                return msg.say(e.message);
            }
        });
    }
}
exports.default = RandomDogsCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9ncy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21tYW5kcy9SYW5kb20vZG9ncy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLDZEQUErRTtBQUUvRSxrREFBNkM7QUFFN0MsTUFBcUIsaUJBQWtCLFNBQVEsNkJBQU87SUFDbEQsWUFBWSxNQUFzQjtRQUM5QixLQUFLLENBQUMsTUFBTSxFQUFFO1lBQ1YsSUFBSSxFQUFFLEtBQUs7WUFDWCxPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDO1lBQ2hDLEtBQUssRUFBRSxRQUFRO1lBQ2YsVUFBVSxFQUFFLEtBQUs7WUFDakIsV0FBVyxFQUFFLGdCQUFnQjtTQUNoQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUssR0FBRyxDQUFDLEdBQW9COztZQUMxQixJQUFJO2dCQUNBLE1BQU0sUUFBUSxHQUFrQixNQUFNLGVBQUssQ0FBQyxHQUFHLENBQUMseUNBQXlDLENBQUMsQ0FBQztnQkFDM0YsTUFBTSxPQUFPLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUM1QyxNQUFNLEdBQUcsQ0FBQyxHQUFHLENBQUM7b0JBQ1YsS0FBSyxFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7aUJBQ2pDLENBQUMsQ0FBQztnQkFDSCxNQUFNLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUE2QixFQUFFLEVBQUU7b0JBRWpELFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDdEIsQ0FBQyxDQUFDLENBQUM7YUFDTjtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNSLE9BQU8sR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDN0I7UUFDTCxDQUFDO0tBQUE7Q0FDSjtBQTFCRCxvQ0EwQkMifQ==