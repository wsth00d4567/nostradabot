"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const axios_1 = __importDefault(require("axios"));
module.exports = class RandomMemeCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "meme",
            aliases: ["random_meme", "memes", "troll"],
            group: "random",
            memberName: "meme",
            description: "GIVES U EZ CAT",
            throttling: {
                usages: 1,
                duration: 5,
            }
        });
    }
    run(msg) {
        function getMemes() {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const response = yield axios_1.default.get("https://meme-api.herokuapp.com/gimme");
                    const message = msg.say("LOOKING FOR MAYMAYZ");
                    yield msg.say({
                        files: [response.data.url]
                    });
                    message.then((res) => res.delete());
                }
                catch (e) {
                    return msg.say(e.message + e.code);
                }
            });
        }
        return getMemes();
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVtZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21tYW5kcy9SYW5kb20vbWVtZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLDZEQUErRTtBQUUvRSxrREFBMkM7QUFFM0MsTUFBTSxDQUFDLE9BQU8sR0FBRyxNQUFNLGlCQUFrQixTQUFRLDZCQUFPO0lBQ3BELFlBQVksTUFBc0I7UUFDOUIsS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNWLElBQUksRUFBRSxNQUFNO1lBQ1osT0FBTyxFQUFFLENBQUMsYUFBYSxFQUFFLE9BQU8sRUFBRSxPQUFPLENBQUM7WUFDMUMsS0FBSyxFQUFFLFFBQVE7WUFDZixVQUFVLEVBQUUsTUFBTTtZQUNsQixXQUFXLEVBQUUsZ0JBQWdCO1lBQzdCLFVBQVUsRUFBRTtnQkFDUixNQUFNLEVBQUUsQ0FBQztnQkFDVCxRQUFRLEVBQUUsQ0FBQzthQUNkO1NBQ0osQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELEdBQUcsQ0FBQyxHQUFvQjtRQUNwQixTQUFlLFFBQVE7O2dCQUNuQixJQUFJO29CQUNBLE1BQU0sUUFBUSxHQUFrQixNQUFNLGVBQUssQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztvQkFDeEYsTUFBTSxPQUFPLEdBQWlDLEdBQUcsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztvQkFDN0UsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDO3dCQUNWLEtBQUssRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO3FCQUM3QixDQUFDLENBQUM7b0JBRUgsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQXdCLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO2lCQUM1RDtnQkFBQyxPQUFPLENBQUMsRUFBRTtvQkFDUixPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3RDO1lBQ0wsQ0FBQztTQUFBO1FBQ0QsT0FBTyxRQUFRLEVBQUUsQ0FBQztJQUN0QixDQUFDO0NBQ0osQ0FBQSJ9