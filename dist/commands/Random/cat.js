"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const axios_1 = __importDefault(require("axios"));
class CatImageCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "cat",
            aliases: ["random_cat", "saad"],
            group: "random",
            memberName: "cat",
            description: "GIVES U EZ CAT",
            argsCount: 0
        });
    }
    run(msg) {
        function getRandomCatImagesFromApi() {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const response = yield axios_1.default.get("https://aws.random.cat/meow");
                    const message = msg.say("LOOKING FOR SAAD");
                    yield msg.say({
                        files: [response.data.file]
                    });
                    message.then((response) => response.delete());
                }
                catch (e) {
                    return msg.say(e.message + e.code);
                }
            });
        }
        return getRandomCatImagesFromApi();
    }
}
exports.default = CatImageCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2F0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmRzL1JhbmRvbS9jYXQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFBQSw2REFBK0U7QUFFL0Usa0RBQTZDO0FBRTdDLE1BQXFCLGVBQWdCLFNBQVEsNkJBQU87SUFDaEQsWUFBWSxNQUFzQjtRQUM5QixLQUFLLENBQUMsTUFBTSxFQUFFO1lBQ1YsSUFBSSxFQUFFLEtBQUs7WUFDWCxPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDO1lBQy9CLEtBQUssRUFBRSxRQUFRO1lBQ2YsVUFBVSxFQUFFLEtBQUs7WUFDakIsV0FBVyxFQUFFLGdCQUFnQjtZQUM3QixTQUFTLEVBQUUsQ0FBQztTQUNmLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxHQUFHLENBQUMsR0FBb0I7UUFDcEIsU0FBZSx5QkFBeUI7O2dCQUNyQyxJQUFJO29CQUNILE1BQU0sUUFBUSxHQUFrQixNQUFNLGVBQUssQ0FBQyxHQUFHLENBQUMsNkJBQTZCLENBQUMsQ0FBQztvQkFDL0UsTUFBTSxPQUFPLEdBQWlDLEdBQUcsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztvQkFDMUUsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDO3dCQUNWLEtBQUssRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO3FCQUM5QixDQUFDLENBQUM7b0JBRUgsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQTZCLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO2lCQUNuRTtnQkFBQyxPQUFNLENBQUMsRUFBRTtvQkFDTixPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3ZDO1lBQ0osQ0FBQztTQUFBO1FBQ0QsT0FBTyx5QkFBeUIsRUFBRSxDQUFDO0lBQ3ZDLENBQUM7Q0FDSjtBQTVCRCxrQ0E0QkMifQ==