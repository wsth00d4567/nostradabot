"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
require("dotenv").config({ path: "../../Utils/.env" });
const axios_1 = __importDefault(require("axios"));
class RandomBirbFaxCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "birbfax",
            aliases: ["birb_facts", "facts_birb"],
            group: "fax",
            memberName: "birbfax",
            description: "GIVES U EZ CAT",
        });
    }
    run(msg) {
        function getBirbFax() {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const response = yield axios_1.default.get('https://some-random-api.ml/facts/bird');
                    const message = msg.say("SEARCHING FOR BIRB FAX");
                    yield msg.say(response.data.fact);
                    message.then((res) => res.delete());
                }
                catch (e) {
                    return msg.say(e.message + e.code);
                }
            });
        }
        return getBirbFax();
    }
}
exports.default = RandomBirbFaxCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmlyYmZheC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21tYW5kcy9GYXgvYmlyYmZheC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLDZEQUErRTtBQUUvRSxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUMsSUFBSSxFQUFFLGtCQUFrQixFQUFDLENBQUMsQ0FBQztBQUNyRCxrREFBNkM7QUFFN0MsTUFBcUIsb0JBQXFCLFNBQVEsNkJBQU87SUFDckQsWUFBWSxNQUFzQjtRQUM5QixLQUFLLENBQUMsTUFBTSxFQUFFO1lBQ1YsSUFBSSxFQUFFLFNBQVM7WUFDZixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDO1lBQ3JDLEtBQUssRUFBRSxLQUFLO1lBQ1osVUFBVSxFQUFFLFNBQVM7WUFDckIsV0FBVyxFQUFFLGdCQUFnQjtTQUNoQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsR0FBRyxDQUFDLEdBQW9CO1FBQ3BCLFNBQWUsVUFBVTs7Z0JBQ3JCLElBQUk7b0JBQ0EsTUFBTSxRQUFRLEdBQWtCLE1BQU0sZUFBSyxDQUFDLEdBQUcsQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO29CQUN6RixNQUFNLE9BQU8sR0FBaUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO29CQUNoRixNQUFNLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFFbEMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQXdCLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO2lCQUM1RDtnQkFBQyxPQUFNLENBQUMsRUFBRTtvQkFDUixPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ3JDO1lBQ0wsQ0FBQztTQUFBO1FBQ0QsT0FBTyxVQUFVLEVBQUUsQ0FBQztJQUN4QixDQUFDO0NBQ0o7QUF6QkQsdUNBeUJDIn0=