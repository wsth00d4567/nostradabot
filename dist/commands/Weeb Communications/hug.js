"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const discord_js_1 = require("discord.js");
const axios_1 = __importDefault(require("axios"));
class HugGifCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "hug",
            aliases: ["hug_gif", "hugme", "3n9"],
            group: "gif",
            memberName: "hug",
            description: "HUGS THE MENTIONED USER",
            details: "HUGS THE MENTIONED USER AND IF THE MENTIONED USER'S ID EQUALS THE MESSAGE AUTHOR'S ID THEN NOSTRADABOT REPLIES WITH ***AWWWWWWN SORRY TO RUIN IT FOR YOU BUD BUT YOU CAN'T HUG YOURSELF I CAN HUG YOU INSTEAD :3*** ELSE NOSTRADABOT REPLIES ***THE MESSAGE AUTHOR HUGGED THE MENTIONED USER***",
            clientPermissions: ["EMBED_LINKS", "SEND_MESSAGES"],
            args: [
                {
                    key: "user",
                    type: "user",
                    prompt: "WHO DO YOU WANNA HUG BRUV?"
                }
            ]
        });
    }
    run(msg, args) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const response = yield axios_1.default.get("https://some-random-api.ml/animu/hug");
                const user = args.user;
                let message;
                if (user.id === msg.author.id) {
                    const embed = new discord_js_1.MessageEmbed()
                        .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: "webp" }))
                        .setTitle("AWWWWWWN SORRY TO RUIN IT FOR YOU BUD BUT YOU CAN'T HUG YOURSELF I CAN HUG YOU INSTEAD :3")
                        .setImage(response.data.link)
                        .setFooter(user.username, user.displayAvatarURL({ format: "webp" }));
                    yield msg.embed(embed);
                }
                else if (user.id !== msg.author.id && user.id !== this.client.user.id) {
                    message = `${msg.author.username} JUST HUGGED ${user.username}`;
                    const embed = new discord_js_1.MessageEmbed()
                        .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: "webp" }))
                        .setTitle(message)
                        .setImage(response.data.link)
                        .setFooter(user.username, user.displayAvatarURL({ format: "webp" }));
                    yield msg.embed(embed);
                }
                else if (user.id === this.client.user.id) {
                    message = `AWWWWWWWWWWWWN THANK YOU WHGE9URHEIHGIREHGIRHGNTRIHT`;
                    const embed = new discord_js_1.MessageEmbed()
                        .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: "webp" }))
                        .setTitle(message)
                        .setImage(response.data.link)
                        .setFooter(`HE HUGGED MEEEEEEEEEEEEEEE`);
                    yield msg.embed(embed);
                }
            }
            catch (e) {
                return msg.say(e.message);
            }
        });
    }
}
exports.default = HugGifCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHVnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmRzL1dlZWIgQ29tbXVuaWNhdGlvbnMvaHVnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQUEsNkRBQStFO0FBQy9FLDJDQUF5RDtBQUN6RCxrREFBNkM7QUFNN0MsTUFBcUIsYUFBYyxTQUFRLDZCQUFPO0lBQzlDLFlBQW1CLE1BQXNCO1FBQ3JDLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDVixJQUFJLEVBQUUsS0FBSztZQUNYLE9BQU8sRUFBRSxDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsS0FBSyxDQUFDO1lBQ3BDLEtBQUssRUFBRSxLQUFLO1lBQ1osVUFBVSxFQUFFLEtBQUs7WUFDakIsV0FBVyxFQUFFLHlCQUF5QjtZQUN0QyxPQUFPLEVBQ0gsaVNBQWlTO1lBQ3JTLGlCQUFpQixFQUFFLENBQUMsYUFBYSxFQUFFLGVBQWUsQ0FBQztZQUNuRCxJQUFJLEVBQUU7Z0JBQ0Y7b0JBQ0ksR0FBRyxFQUFFLE1BQU07b0JBQ1gsSUFBSSxFQUFFLE1BQU07b0JBQ1osTUFBTSxFQUFFLDRCQUE0QjtpQkFDdkM7YUFDSjtTQUNKLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFSyxHQUFHLENBQUMsR0FBb0IsRUFBRSxJQUFhOztZQUN6QyxJQUFJO2dCQUNBLE1BQU0sUUFBUSxHQUFrQixNQUFNLGVBQUssQ0FBQyxHQUFHLENBQzNDLHNDQUFzQyxDQUN6QyxDQUFDO2dCQUNGLE1BQU0sSUFBSSxHQUFTLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQzdCLElBQUksT0FBZSxDQUFDO2dCQUNwQixJQUFJLElBQUksQ0FBQyxFQUFFLEtBQUssR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUU7b0JBQzNCLE1BQU0sS0FBSyxHQUFHLElBQUkseUJBQVksRUFBRTt5QkFDM0IsU0FBUyxDQUNOLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUNuQixHQUFHLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQ2xEO3lCQUNBLFFBQVEsQ0FDTCwyRkFBMkYsQ0FDOUY7eUJBQ0EsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO3lCQUM1QixTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUN6RSxNQUFNLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzFCO3FCQUFNLElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRTtvQkFDckUsT0FBTyxHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLGdCQUFnQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ2hFLE1BQU0sS0FBSyxHQUFHLElBQUkseUJBQVksRUFBRTt5QkFDM0IsU0FBUyxDQUNOLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUNuQixHQUFHLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQ2xEO3lCQUNBLFFBQVEsQ0FBQyxPQUFPLENBQUM7eUJBQ2pCLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzt5QkFDNUIsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDekUsTUFBTSxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUMxQjtxQkFBTSxJQUFJLElBQUksQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFO29CQUN4QyxPQUFPLEdBQUcsc0RBQXNELENBQUM7b0JBQ2pFLE1BQU0sS0FBSyxHQUFHLElBQUkseUJBQVksRUFBRTt5QkFDM0IsU0FBUyxDQUNOLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUNuQixHQUFHLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQ2xEO3lCQUNBLFFBQVEsQ0FBQyxPQUFPLENBQUM7eUJBQ2pCLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzt5QkFDNUIsU0FBUyxDQUFDLDRCQUE0QixDQUFDLENBQUM7b0JBQzdDLE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDMUI7YUFDSjtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNSLE9BQU8sR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDN0I7UUFDTCxDQUFDO0tBQUE7Q0FDSjtBQW5FRCxnQ0FtRUMifQ==