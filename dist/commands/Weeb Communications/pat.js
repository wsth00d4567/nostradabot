"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const discord_js_1 = require("discord.js");
const axios_1 = __importDefault(require("axios"));
class PatGifCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "pat",
            aliases: ["pat_gif", "patme"],
            group: "gif",
            memberName: "pat",
            description: "PATS THE MENTIONED USER",
            clientPermissions: ["EMBED_LINKS", "SEND_MESSAGES"],
            argsCount: 1,
            args: [
                {
                    key: "user",
                    type: "user",
                    prompt: "WHO?",
                    default: (msg) => msg.author
                }
            ]
        });
    }
    run(msg, args) {
        function getRandomPatGifFromApi() {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const response = yield axios_1.default.get("https://some-random-api.ml/animu/pat");
                    const user = args.user;
                    let message;
                    if (user.id === msg.author.id) {
                        message = `${msg.author.username} JUST PAT HIMSELF. LOL FUCKING VIRGIN`;
                    }
                    else {
                        message = `${user.username} JUST GOT PAT BY ${msg.author.username}`;
                    }
                    const embed = new discord_js_1.MessageEmbed()
                        .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: "webp" }))
                        .setTitle(message)
                        .setImage(response.data.link)
                        .setFooter(user.username, user.displayAvatarURL({ format: "webp" }));
                    yield msg.embed(embed);
                }
                catch (e) {
                    return msg.say(e.message);
                }
            });
        }
        return getRandomPatGifFromApi();
    }
}
exports.default = PatGifCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2NvbW1hbmRzL1dlZWIgQ29tbXVuaWNhdGlvbnMvcGF0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQUEsNkRBQStFO0FBQy9FLDJDQUF5RDtBQUN6RCxrREFBNkM7QUFNN0MsTUFBcUIsYUFBYyxTQUFRLDZCQUFPO0lBQzlDLFlBQVksTUFBc0I7UUFDOUIsS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNWLElBQUksRUFBRSxLQUFLO1lBQ1gsT0FBTyxFQUFFLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQztZQUM3QixLQUFLLEVBQUUsS0FBSztZQUNaLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLFdBQVcsRUFBRSx5QkFBeUI7WUFDdEMsaUJBQWlCLEVBQUUsQ0FBQyxhQUFhLEVBQUUsZUFBZSxDQUFDO1lBQ25ELFNBQVMsRUFBRSxDQUFDO1lBQ1osSUFBSSxFQUFFO2dCQUNGO29CQUNJLEdBQUcsRUFBRSxNQUFNO29CQUNYLElBQUksRUFBRSxNQUFNO29CQUNaLE1BQU0sRUFBRSxNQUFNO29CQUNkLE9BQU8sRUFBRSxDQUFDLEdBQW9CLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNO2lCQUNoRDthQUNKO1NBQ0osQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELEdBQUcsQ0FBQyxHQUFvQixFQUFFLElBQWE7UUFDbkMsU0FBZSxzQkFBc0I7O2dCQUNqQyxJQUFJO29CQUNBLE1BQU0sUUFBUSxHQUFrQixNQUFNLGVBQUssQ0FBQyxHQUFHLENBQUMsc0NBQXNDLENBQUMsQ0FBQztvQkFDeEYsTUFBTSxJQUFJLEdBQVMsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFDN0IsSUFBSSxPQUFlLENBQUM7b0JBQ3BCLElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRTt3QkFDM0IsT0FBTyxHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLHVDQUF1QyxDQUFDO3FCQUMzRTt5QkFBTTt3QkFDSCxPQUFPLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxvQkFBb0IsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkU7b0JBQ0QsTUFBTSxLQUFLLEdBQUcsSUFBSSx5QkFBWSxFQUFFO3lCQUMzQixTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO3lCQUMvRSxRQUFRLENBQUMsT0FBTyxDQUFDO3lCQUNqQixRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7eUJBQzVCLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3pFLE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDMUI7Z0JBQUMsT0FBTyxDQUFDLEVBQUU7b0JBQ1IsT0FBTyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDN0I7WUFDTCxDQUFDO1NBQUE7UUFDRCxPQUFPLHNCQUFzQixFQUFFLENBQUM7SUFDcEMsQ0FBQztDQUNKO0FBNUNELGdDQTRDQyJ9