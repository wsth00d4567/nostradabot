"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_commando_1 = require("discord.js-commando");
const discord_js_1 = require("discord.js");
const axios_1 = __importDefault(require("axios"));
class WinkGifCommand extends discord_js_commando_1.Command {
    constructor(client) {
        super(client, {
            name: "wink",
            aliases: ["winkat", "ghmz", "ghmiza"],
            group: "gif",
            memberName: "wink",
            description: "WINKS AT THE MENTIONED USER",
            details: `WINKS AT THE MENTIONED USER AND IF THE MENTIONED USER'S ID IS AS SAME AS THE MESSAGE AUTHOR'S THEN NOSTRADABOT REPLIES WITH ***AS MUCH AS U LOVE YOURSELF YOU CAN'T WINK AT YOURSELF*** AND IF IT ISN'T NOSTRADABOT REPLIES WITH *** THE MESSAGE AUTHOR WINKED AT THE MENTIONED USER***`,
            clientPermissions: ["EMBED_LINKS", "SEND_MESSAGES"],
            argsCount: 1,
            args: [
                {
                    key: "user",
                    type: "user",
                    prompt: "WHOM",
                    default: (msg) => msg.author
                }
            ]
        });
    }
    run(msg, args) {
        function getWinkGifs() {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    let reply;
                    const user = args.user;
                    const response = yield axios_1.default.get('https://some-random-api.ml/animu/wink');
                    const message = msg.say("SEARCHING FOR WINKIES");
                    if (user.id === msg.author.id) {
                        reply = "AS MUCH AS U LOVE YOURSELF YOU CAN'T WINK AT YOURSELF";
                    }
                    else {
                        reply = `${msg.author.username} WINKED AT ${user.username}`;
                    }
                    const embed = new discord_js_1.MessageEmbed()
                        .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: 'webp' }))
                        .setTitle(reply)
                        .setImage(response.data.link);
                    yield msg.embed(embed);
                    message.then((res) => res.delete());
                }
                catch (e) {
                    return msg.say(e.message + e.code);
                }
            });
        }
        return getWinkGifs();
    }
}
exports.default = WinkGifCommand;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2luay5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9jb21tYW5kcy9XZWViIENvbW11bmljYXRpb25zL3dpbmsudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFBQSw2REFBK0U7QUFDL0UsMkNBQXlEO0FBQ3pELGtEQUE2QztBQU03QyxNQUFxQixjQUFlLFNBQVEsNkJBQU87SUFDL0MsWUFBWSxNQUFzQjtRQUM5QixLQUFLLENBQUMsTUFBTSxFQUFFO1lBQ1YsSUFBSSxFQUFFLE1BQU07WUFDWixPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQztZQUNyQyxLQUFLLEVBQUUsS0FBSztZQUNaLFVBQVUsRUFBRSxNQUFNO1lBQ2xCLFdBQVcsRUFBRSw2QkFBNkI7WUFDMUMsT0FBTyxFQUFFLHlSQUF5UjtZQUNsUyxpQkFBaUIsRUFBRSxDQUFDLGFBQWEsRUFBRSxlQUFlLENBQUM7WUFDbkQsU0FBUyxFQUFFLENBQUM7WUFDWixJQUFJLEVBQUU7Z0JBQ0Y7b0JBQ0ksR0FBRyxFQUFFLE1BQU07b0JBQ1gsSUFBSSxFQUFFLE1BQU07b0JBQ1osTUFBTSxFQUFFLE1BQU07b0JBQ2QsT0FBTyxFQUFFLENBQUMsR0FBb0IsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU07aUJBQ2hEO2FBQ0o7U0FDSixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsR0FBRyxDQUFDLEdBQW9CLEVBQUUsSUFBYztRQUNwQyxTQUFlLFdBQVc7O2dCQUN0QixJQUFJO29CQUNBLElBQUksS0FBYSxDQUFDO29CQUNsQixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUN2QixNQUFNLFFBQVEsR0FBa0IsTUFBTSxlQUFLLENBQUMsR0FBRyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7b0JBQ3pGLE1BQU0sT0FBTyxHQUFpQyxHQUFHLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7b0JBQy9FLElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRTt3QkFDM0IsS0FBSyxHQUFHLHVEQUF1RCxDQUFDO3FCQUNuRTt5QkFBTTt3QkFDSCxLQUFLLEdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsY0FBYyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7cUJBQy9EO29CQUNELE1BQU0sS0FBSyxHQUFHLElBQUkseUJBQVksRUFBRTt5QkFDM0IsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQzt5QkFDL0UsUUFBUSxDQUFDLEtBQUssQ0FBQzt5QkFDZixRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDbEMsTUFBTSxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUV2QixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBd0IsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7aUJBQzVEO2dCQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNSLE9BQU8sR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDdEM7WUFDTCxDQUFDO1NBQUE7UUFDRCxPQUFPLFdBQVcsRUFBRSxDQUFDO0lBQ3pCLENBQUM7Q0FDSjtBQS9DRCxpQ0ErQ0MifQ==