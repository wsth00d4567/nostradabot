import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message } from 'discord.js';
require("dotenv").config({path: "../../Utils/.env"});
import axios, { AxiosResponse } from 'axios';

export default class RandomBirbFaxCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: "birbfax",
            aliases: ["birb_facts", "facts_birb"],
            group: "fax",
            memberName: "birbfax",
            description: "GIVES U EZ CAT",
        });
    }

    run(msg: CommandoMessage): Promise<Message | Message[]> {
        async function getBirbFax() {
            try {
                const response: AxiosResponse = await axios.get('https://some-random-api.ml/facts/bird');
                const message: Promise<Message | Message[]> = msg.say("SEARCHING FOR BIRB FAX");
                await msg.say(response.data.fact);
                //@ts-ignore
                message.then((res: Message | Message[]) => res.delete());
            } catch(e) {
               return msg.say(e.message + e.code);
            }
        } 
        return getBirbFax();
    }
}
