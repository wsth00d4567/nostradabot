import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message, MessageEmbed, User, Role } from 'discord.js';

interface IUserArg {
    user?: User;
}

export default class UserInfoCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: "user",
            aliases: ["me", "user_info", "him"],
            group: "userinfo",
            memberName: "user",
            description: "SEE A USER'S INFORMATION EZ",
            guildOnly: true,
            args: [
                {
                    key: "user",
                    type: "user",
                    prompt: "who?",
                    default: (msg: CommandoMessage) => msg.author
                }
            ]
        });
    }

    async run(msg: CommandoMessage, args: IUserArg): Promise<Message | Message[]> {
        const user: User = args.user;
        const embed = new MessageEmbed()
              .setAuthor(`${user.username}`, user.displayAvatarURL({ format: 'webp' }))
              .setTitle(`${user.username}'S INFROMATION`)
              .setThumbnail(user.displayAvatarURL   ({ format: 'png', size: 2048 }))
              .addField("ISB0T?", user.bot ? 'YES' : 'NAH', true)
              .addField("ACCOUNT CREATED AT", user.createdAt.toUTCString(), true)
              .addField("USER DISCRIMINATOR", user.discriminator, true)
              .addField("USER ID", user.id, true)
              .addField("USER PRESENCE", user.presence.status.toUpperCase(), true)
              .addField("USER TAG", user.tag.toUpperCase(), true)
    try {
        const member = await msg.guild.members.fetch(user.id);
        if (!member) {
            msg.reply("SORRY BRO BUT THIS MEMBER EITHER GOTTEN DELETED OR BANNED FROM THE GUILD IF YOU THINK THIS IS AN ERROR PLEASE CONTACT THE DEVELOPER " + "STEPHANIE#4687");
        } else {
            const roles = member.roles
                                    .filter((role: Role) => role.name !== "@everyone")
                                    .sort((a: Role, b: Role) => b.position - a.position)
                                    .map((role: Role) => role.name);
            embed
            .setColor(member.displayHexColor)
            .addField("USER BANNABLE?", member.bannable ? 'YES' : 'NOPE', true) 
            .addField("USER NICKNAME", member.nickname || "NONE", true)  
            .addField("USER'S HIGHEST ROLE", member.roles.highest.name === '@everyone' ? 'THIS USER GOT NO ROLES' : member.roles.highest.name, true)
            .addField("USER'S HOIST ROLE", member.roles.hoist ? member.roles.hoist.name : 'THIS USER GOT NO HOIST ROLES', true)
            .addField(`USER'S ROLES ${roles.length}`, roles.join(', '));
        }
    } catch (error) {
        embed.setFooter("THIS IS WHAT I COULD FETCH FROM THE MEMBER :(");
    }  
        return msg.embed(embed);
    }
}