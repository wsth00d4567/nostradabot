import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message, User, ImageSize, MessageAttachment, ImageExt, MessageEmbed } from 'discord.js';

interface IAvatarCommandArgs {
    user?: User;
    size?: ImageSize;
    format?: ImageExt;
}

export default class AvatarCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: "avatar",
            aliases: ["pfp", "pdp", "user_avatar", "user_pfp"],
            group: "userinfo",
            memberName: "avatar",
            description: "SEE AND DOWNLOAD A USER'S AVATAR",
            throttling: {
                usages: 1,
                duration: 4
            },
            args: [
                {
                    key: "user",
                    prompt: "whom",
                    type: "user",
                    default: (msg: CommandoMessage) => msg.author
                },
                {
                    key: "size",
                    prompt: "nani",
                    type: "integer",
                    oneOf: [ 16, 32, 64, 128, 256, 512, 1024, 2048 ],
                    default: 2048                 
                },
                {
                    key: "format",
                    prompt: "what format",
                    type: "string",
                    oneOf: [ "webp", "png", "jpg", "gif" ],
                    default: 'png'
                }
            ]
        });
    }

    async run(msg: CommandoMessage, args: IAvatarCommandArgs): Promise<Message | Message[]> {
        const image: MessageAttachment = new MessageAttachment(args.user.displayAvatarURL({ size: args.size, format: args.format }));
        await msg.channel.send(image);
        let embed = new MessageEmbed()
            .setDescription(`IF YOU WANT TO DOWNLOAD THE AVATAR AS [${args.format.toString().toUpperCase()}](${args.user.displayAvatarURL({ format: args.format, size: args.size }).toString()})`);
        return msg.embed(embed);
    }
}