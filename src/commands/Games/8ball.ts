import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message, MessageEmbed } from 'discord.js';
let answers = require("../../../API/JSON/8Ball.json");
const { eightballIcon } = require('../../../API/GLOBALS/globalvars.js');

interface I8BallArgs {
  question: string;
}

export default class EightBallCommand extends Command {
  constructor(client: CommandoClient) {
    super(client, {
      name: "8ball",
      aliases: ["eightball"],
      group: "fun",
      memberName: "8ball",
      description: "SEE A BOT'S INFORMATION EZ",
      args: [
        {
          key: "question",
          type: "string",
          prompt: "WHAT DO YOU WANNA ASK MEEEEEEEEE",
        }
      ]
    });
  }

  async run(msg: CommandoMessage, args: I8BallArgs): Promise<Message | Message[]> {
    const embed = new MessageEmbed()
          .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: 'webp' }))
          .setThumbnail(eightballIcon)
          .setTitle(args.question)
          .addField('ANSWER', `**${answers[Math.floor(Math.random() * answers.length)]}**`);
    return msg.embed(embed);
  }
}
