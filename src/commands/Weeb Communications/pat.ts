import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message, MessageEmbed, User } from 'discord.js';
import axios, { AxiosResponse } from "axios";

interface IPatArg {
    user?: User;
}

export default class PatGifCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: "pat",
            aliases: ["pat_gif", "patme"],
            group: "gif",
            memberName: "pat",
            description: "PATS THE MENTIONED USER",
            clientPermissions: ["EMBED_LINKS", "SEND_MESSAGES"],
            argsCount: 1,
            args: [
                {
                    key: "user",
                    type: "user",
                    prompt: "WHO?",
                    default: (msg: CommandoMessage) => msg.author
                }
            ]
        });
    }

    run(msg: CommandoMessage, args: IPatArg): Promise<Message | Message[]> {
        async function getRandomPatGifFromApi() {
            try {
                const response: AxiosResponse = await axios.get("https://some-random-api.ml/animu/pat");
                const user: User = args.user;
                let message: string;
                if (user.id === msg.author.id) {
                    message = `${msg.author.username} JUST PAT HIMSELF. LOL FUCKING VIRGIN`;
                } else {
                    message = `${user.username} JUST GOT PAT BY ${msg.author.username}`;
                }
                const embed = new MessageEmbed()
                    .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: "webp" }))
                    .setTitle(message)
                    .setImage(response.data.link)
                    .setFooter(user.username, user.displayAvatarURL({ format: "webp" }));
                await msg.embed(embed);
            } catch (e) {
                return msg.say(e.message);
            }
        }
        return getRandomPatGifFromApi();
    }
}
