import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message, User, MessageEmbed } from 'discord.js';
import axios, { AxiosResponse } from 'axios';

interface IWinkArg {
    user?: User;
}

export default class WinkGifCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: "wink",
            aliases: ["winkat", "ghmz", "ghmiza"],
            group: "gif",
            memberName: "wink",
            description: "WINKS AT THE MENTIONED USER",
            details: `WINKS AT THE MENTIONED USER AND IF THE MENTIONED USER'S ID IS AS SAME AS THE MESSAGE AUTHOR'S THEN NOSTRADABOT REPLIES WITH ***AS MUCH AS U LOVE YOURSELF YOU CAN'T WINK AT YOURSELF*** AND IF IT ISN'T NOSTRADABOT REPLIES WITH *** THE MESSAGE AUTHOR WINKED AT THE MENTIONED USER***`,
            clientPermissions: ["EMBED_LINKS", "SEND_MESSAGES"],
            argsCount: 1,
            args: [
                {
                    key: "user",
                    type: "user",
                    prompt: "WHOM",
                    default: (msg: CommandoMessage) => msg.author
                }
            ]
        });
    }

    run(msg: CommandoMessage, args: IWinkArg): Promise<Message | Message[]> {
        async function getWinkGifs() {
            try {
                let reply: string;
                const user = args.user;
                const response: AxiosResponse = await axios.get('https://some-random-api.ml/animu/wink');
                const message: Promise<Message | Message[]> = msg.say("SEARCHING FOR WINKIES");
                if (user.id === msg.author.id) {
                    reply = "AS MUCH AS U LOVE YOURSELF YOU CAN'T WINK AT YOURSELF";
                } else {
                    reply = `${msg.author.username} WINKED AT ${user.username}`;
                }
                const embed = new MessageEmbed()
                    .setAuthor(msg.author.username, msg.author.displayAvatarURL({ format: 'webp' }))
                    .setTitle(reply)
                    .setImage(response.data.link);
                await msg.embed(embed);
                //@ts-ignore
                message.then((res: Message | Message[]) => res.delete());
            } catch (e) {
                return msg.say(e.message + e.code);
            }
        }
        return getWinkGifs();
    }
}
