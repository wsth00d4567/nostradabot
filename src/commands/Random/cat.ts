import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message } from 'discord.js';
import axios, { AxiosResponse } from 'axios';

export default class CatImageCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: "cat",
            aliases: ["random_cat", "saad"],
            group: "random",
            memberName: "cat",
            description: "GIVES U EZ CAT",
            argsCount: 0
        });
    }

    run(msg: CommandoMessage): Promise<Message | Message[]> {
        async function getRandomCatImagesFromApi() {
           try {
            const response: AxiosResponse = await axios.get("https://aws.random.cat/meow");
            const message: Promise<Message | Message[]> = msg.say("LOOKING FOR SAAD");
            await msg.say({
                files: [response.data.file]
            });
            //@ts-ignore
            message.then((response: Message | Message[]) => response.delete());
           } catch(e) {
                return msg.say(e.message + e.code);
           }
        }
        return getRandomCatImagesFromApi();
    }
}
