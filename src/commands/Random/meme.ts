import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { Message } from 'discord.js';
import axios, {AxiosResponse} from 'axios';

module.exports = class RandomMemeCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: "meme",
            aliases: ["random_meme", "memes", "troll"],
            group: "random",
            memberName: "meme",
            description: "GIVES U EZ CAT",
            throttling: {
                usages: 1,
                duration: 5,
            }
        });
    }

    run(msg: CommandoMessage): Promise<Message | Message[]> {
        async function getMemes() {
            try {
                const response: AxiosResponse = await axios.get("https://meme-api.herokuapp.com/gimme");
                const message: Promise<Message | Message[]> = msg.say("LOOKING FOR MAYMAYZ");
                await msg.say({
                    files: [response.data.url]
                });
                //@ts-ignore
                message.then((res: Message | Message[]) => res.delete());
            } catch (e) {
                return msg.say(e.message + e.code);
            }
        }
        return getMemes();
    }
}

